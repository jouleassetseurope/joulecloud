#Y1 Status
df1 <- fread(file=file.choose(),sep=',',header=TRUE) 
#Power values 
df2 <- fread(file=file.choose(),sep=',',header=TRUE)

#Removing duplicates
df_power_final<-unique(df1, by = "Time_Power")

#Formatting the dates/time
df_power_final$Time_Power <- as.POSIXct(df_power_final$Time_Power, format="%m/%d/%Y %T")

df2$Time_Gfan <- as.POSIXct(df2$Time_Gfan, format="%m/%d/%Y %T",tz="")

#Setting the keys up
setkey(df_power_final, Time_Power)

setkey(df2, Time_Gfan)

#Merging two
new_data_power_gfan<-df2[df_power_final, roll=Inf]
